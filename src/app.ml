open Ballot

type model = ballot_rule

type msg = SaveBallotType of ballot_type [@@bs.deriving {accessors}]

let msg_to_string (msg : msg) =
  match msg with SaveBallotType _ballot_type -> "changing ballot type"

let update model = function
  | SaveBallotType ballot_type ->
      ({model with ballot_type}, Tea.Cmd.none)

let init () =
  let ballot_rule : Ballot.ballot_rule =
    { ballot_type= SingleChoice
    ; choices= ["yes"; "no"]
    ; starting_threshold= [Participation (Absolute 3)]
    ; validity_threshold= [Participation (Relative 0.6)]
    ; ending_threshold= [Duration (Week 1)]
    ; invalidity_threshold= []
    ; winning_threshold= [("yes", Relative 0.66)] }
  in
  (ballot_rule, Tea.Cmd.none)

let view_button button_text msg =
  let open Tea.Html in
  button [onClick msg] [text button_text]

let view model =
  let open Tea.Html in
  div []
    [ span [] [text ("Ballot type: " ^ string_of_ballot_type model.ballot_type)]
    ; div []
        [ text "Choices"
        ; ul [] (List.map (fun c -> li [] [text c]) model.choices) ]
    ; div []
        [ text "Starting threshold"
        ; ul []
            (List.map
               (fun t -> li [] [text (string_of_threshold t)])
               model.starting_threshold ) ]
    ; div []
        [ text "Validity threshold"
        ; ul []
            (List.map
               (fun t -> li [] [text (string_of_threshold t)])
               model.validity_threshold ) ]
    ; div []
        [ text "Ending threshold"
        ; ul []
            (List.map
               (fun t -> li [] [text (string_of_threshold t)])
               model.ending_threshold ) ]
    ; div []
        [ text "Invalidity threshold"
        ; ul []
            (List.map
               (fun t -> li [] [text (string_of_threshold t)])
               model.invalidity_threshold ) ]
    ; div []
        [ text "Winning threshold"
        ; ul []
            (List.map
               (fun (choice, qty) ->
                 li []
                   [ text
                       ("Choice " ^ choice ^ ": " ^ string_of_threshold_qty qty)
                   ] )
               model.winning_threshold ) ] ]

let subscriptions _model = Tea.Sub.none

let shutdown _model = Tea.Cmd.none

let start_app container =
  Tea.App.standardProgram {init; update; view; subscriptions} container ()

let start_debug_app ?(init = init) ?(shutdown = shutdown) container =
  Tea.App.standardProgram {init; update; view; subscriptions} container ()

let start_hot_debug_app container cachedModel =
  (* Replace the existing shutdown function with one that returns the current
   * state of the app, for hot module replacement purposes *)
  (* inspired by https://github.com/walfie/ac-tune-maker *)
  let modelRef = ref None in
  let shutdown model =
    modelRef := Some model ;
    Tea.Cmd.none
  in
  let init =
    match cachedModel with
    | None ->
        init
    | Some model ->
        fun _flags ->
          let _model, cmd = init () in
          (model, cmd)
  in
  let app = start_debug_app ~init ~shutdown container in
  let oldShutdown = app##shutdown in
  let newShutdown () = oldShutdown () ; !modelRef in
  let _ = Js.Obj.assign app [%obj {shutdown= newShutdown}] in
  newShutdown
