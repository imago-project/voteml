(*
    threshold de validité
    threshold de fin
    threshold d'invalidité

    particularité du vote binaire (pour définir une option avec un seuil spécial) ou juste nom d'option

    choix avec veto: InvalidityThreshold (SpecialOption ('non', Absolute 1))
    choix avec veto: InvalidityThreshold (Participation (Relative 0.8))

    - existence d'un buffer de scrutins proposés mais pas encore acceptés?
    use case: on ne veut pas que tout le monde propose des scrutins pour kicker quelqu'un dès qu'ils ne sont pas contents
    donc on veut qu'un vote soit déclenché à partir d'un certain seuil
    => proposition : le starting_threshold ne définit pas le seuil de création du scrutin mais son seuil de visibilité
       => tout le monde peut créer un scrutin, il est seulement invisible au départ



    process:
      règle de
*)

type choice = string

type range_start = int

type range_end = int

type power_level = int

type duration = Month of int | Week of int | Day of int | Hour of int

type threshold_qty = Absolute of int | Relative of float

type threshold =
  | Participation of threshold_qty
  | SpecialChoice of choice * threshold_qty
  | Duration of duration

type ballot_type =
  | SingleChoice
  | OrderedChoice
  | WeightedChoice of range_start * range_end

type ballot_rule =
  { ballot_type: ballot_type
  ; starting_threshold: threshold list
  ; ending_threshold: threshold list
  ; validity_threshold: threshold list
  ; invalidity_threshold: threshold list
  ; winning_threshold: (choice * threshold_qty) list
        (* winning_threshold can only be made of "SpecialChoice thresholds" *)
  ; choices: choice list }

type democracy_rule = Ballot of ballot_rule | MinimumPowerLevel of power_level

type rule = {target: string; process: democracy_rule}

type rule_list = rule list

let string_of_duration = function
  | Month i -> string_of_int i ^ " months"
  | Week i -> string_of_int i ^ " weeks"
  | Day i -> string_of_int i ^ " days"
  | Hour i -> string_of_int i ^ " hours"

let string_of_threshold_qty = function
  | Absolute i -> string_of_int i ^ " votes"
  | Relative f -> string_of_float (f *. 100.0) ^ "% of votes"

let string_of_threshold = function
  | Participation qty -> "Participation: " ^ string_of_threshold_qty qty
  | SpecialChoice (c, qty) -> "Choice " ^ c ^ ": " ^ string_of_threshold_qty qty
  | Duration d -> "Duration: " ^ string_of_duration d

let string_of_ballot_type = function
  | SingleChoice -> "Single choice"
  | OrderedChoice -> "Ordered choice"
  | WeightedChoice (a, b) -> "Weighted choice between " ^ string_of_int a ^ " and " ^ string_of_int b

let example_rules =
  [ { target= "m.room.kick"
    ; process=
        Ballot
          { ballot_type= SingleChoice
          ; choices= ["yes"; "no"]
          ; starting_threshold= [Participation (Absolute 3)]
          ; validity_threshold= [Participation (Relative 0.6)]
          ; ending_threshold= [Duration (Week 1)]
          ; (* invalidity_threshold = [SpecialChoice ("no", Absolute  *)
            invalidity_threshold= []
          ; winning_threshold= [("yes", Relative 0.66)] } } ]
