# Voteml

A try at typing democracy.

## Usage

### Install dependencies

```bash
npm run setup
```

### Run a development server

```bash
npm run dev
```

### Generate a production build

```bash
npm run build
```
